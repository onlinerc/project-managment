
# project-managment

This repository expresses the past and future management plans and decisions for the development of the rc vehicle
Featured in this repository is

* Economics
    * Shipping and possesion costs
    * Fabrication and assembly costs
    * Non standard utility and machining costs

* Scheduling
    * Weekly meeting schedule
    * Development goals timeline
    * Project meetups and events

* Updates
    * Summary of development teams weekly updates
    * Summary of progress with respect to final product
    * Weekly goal status
