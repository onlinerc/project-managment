# Development Schedule

#### The development schedules for all aspects of the projects life

Each schedule expresses specific goals which are to be completed at weekly intervals. Each goal includes a description expressing what is to be accomplished to meet the goal of associated with that week.

The goals are ordered in a linear basis as each goal will require the completion of the previous goal for any new development to be undergone. If a goal is not able to be met by the deadline please speak with a project manager to attempt to create a reconfigured schedule. With the project timeline only consisting of 8 weeks very few schedule reconfigurations can be preformed while still completing the project in the planned timeline.

[Hardware_Schedule]: https://gitlab.com/onlinerc/project-managment/-/raw/main/scheduling/hardware.png
[Electrical_Schedule]: https://gitlab.com/onlinerc/project-managment/-/raw/main/scheduling/electrical.png
[Software_Schedule]: https://gitlab.com/onlinerc/project-managment/-/raw/main/scheduling/software.png

## Hardware Schedule
![Schedule of Hardware][Hardware_Schedule]


## Electrical Schedule
![Schedule of Electrical][Electrical_Schedule]


## Software Schedule
![Schedule of Software][Software_Schedule]
